<?php

namespace App\BookTitle;

use App\Message\Message;
use App\Model\Database as DB;

use App\Utility\Utility;
use PDO;

class BookTitle extends DB
{

    public $id;
    public $book_title;
    public $author_name;


    public function __construct()
    {

        parent::__construct();

    }
    public function setData($postVariableData=NULL)
    {
        if(array_key_exists('id',$postVariableData))
        {
            $this->id=$postVariableData['id'];
        }


        if(array_key_exists('book_title',$postVariableData))
        {
            $this->book_title=$postVariableData['book_title'];
        }
        if(array_key_exists('author_name',$postVariableData))
        {
            $this->author_name=$postVariableData['author_name'];
        }
    }
    public  function  store()
    {
        $arrData=array($this->book_title,$this->author_name);
        $sql="Insert INTO book_title(book_title,author_name) VALUE (?,?)";
        //var_dump($sql);

       $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);
        if($result)
            Message::setMessage("Success! Data has been inserted successfully:)");
        else
            Message::setMessage("Failed !Data has been inserted successfully:(");
        Utility::redirect('crearte.php');
    }//end of store method

    public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from book_title');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();
    public function view($fetchMode='ASSOC')
    {
        $STH = $this->DBH->query('SELECT * from book_title where id='.$this->id);
        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;
    }
    public  function  update()
    {
        $arrData=array($this->book_title,$this->author_name);
        $sql="Update book_title SET book_title=?,author_name=? WHERE id=".$this->id ;
           // UPDATE `avijit_kar_atomic_project_b35`.`book_title` SET `author_name` = 'avijit1' WHERE `book_title`.`id` = 3;
        $STH=$this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('index.php');
    }

    public function delete()

    {

        $sql="Delete from bool_title WHERE  id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('index.php');
    }


}// end of BookTitle class